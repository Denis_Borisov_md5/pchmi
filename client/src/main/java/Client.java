public interface Client {

    boolean isAvailable();

    String getInputData();

    void writeAnswer(String outputData);
}