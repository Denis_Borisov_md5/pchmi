import controller.SerializationController;
import serialization.MediaType;
import serialization.SerializationFacade;
import serialization.types.JSONSerialization;
import service.CalculatorService;
import service.RequestProcessing;

public class BotFactory {
    public Bot createBot(String contextRoot) {
        SerializationFacade serializationFacade = new SerializationFacade();
        RequestProcessing processingFacade = new RequestProcessing(new CalculatorService());
        serializationFacade.registerMediaTypeSerialization(MediaType.JSON, new JSONSerialization());
        SerializationController controller = new SerializationController(serializationFacade, processingFacade);
        Client client = new HTTPClient(contextRoot);
        return new Bot(client, controller);
    }
}