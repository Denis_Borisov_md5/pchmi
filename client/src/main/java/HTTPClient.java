import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPClient implements Client {
    private static final String POST_METHOD = "POST";
    private static final String GET_METHOD = "GET";
    private String contextRoot;

    public HTTPClient(String contextRoot) {
        super();
        this.contextRoot = contextRoot;
    }

    @Override
    public boolean isAvailable() {
        try {
            HttpURLConnection connection = createConnectionForMethod("/ping", GET_METHOD);
            return connection.getResponseCode() == 200;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public String getInputData() {
        try {
            HttpURLConnection connection = createConnectionForMethod("/getinputdata", GET_METHOD);
            verifyResponseCode(connection.getResponseCode());
            return readFromServer(connection);
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override
    public void writeAnswer(String outputData) {
        try {
            HttpURLConnection connection = createConnectionForMethod("/writeanswer", POST_METHOD);
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            int responseCode = sendToServer(connection, outputData);
            verifyResponseCode(responseCode);
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    private HttpURLConnection createConnectionForMethod(String endpoint, String method) throws IOException {
        URL url = new URL(contextRoot + endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        return connection;
    }

    private String readFromServer(HttpURLConnection connection) throws IOException {
        InputStream istream = connection.getInputStream();
        InputStreamReader inputReader = new InputStreamReader(istream);
        BufferedReader bufferedReader = new BufferedReader(inputReader);
        return bufferedReader.lines()
                .reduce((prev, current) -> prev + "\n" + current)
                .get();
    }

    private int sendToServer(HttpURLConnection connection, String data) throws IOException {
        connection.setDoOutput(true);
        try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
            outputStream.writeBytes(data);
            outputStream.flush();
        }
        return connection.getResponseCode();
    }

    private void verifyResponseCode(int responseCode) {
        if (responseCode != 200) {
            throw new IllegalStateException(String.format("Error!!! Error with code %s was got", responseCode));
        }
    }
}