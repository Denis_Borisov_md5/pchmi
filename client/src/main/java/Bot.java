import controller.SerializationController;
import lombok.AllArgsConstructor;
import serialization.MediaType;

@AllArgsConstructor
public class Bot implements Runnable {
    private Client client;
    private SerializationController controller;

    @Override
    public void run() {
        if (client.isAvailable()) {
            try {
                String request = client.getInputData();
                System.out.println("Bot receives input data: " + request);
                String response = controller.handleRequest(MediaType.JSON, request);
                System.out.println("Bot answers to server: " + response);
                client.writeAnswer(response);
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            throw new IllegalStateException("Server is not available");
        }
    }
}