import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BotTest {
    private Bot bot;

    @Before
    public void setUp() {
        BotFactory factory = new BotFactory();
        bot = factory.createBot("http://localhost:8080/study");
    }

    @Test
    public void shouldCompleteOperationWitoutErrorsAndLogInputAndOutput() throws InterruptedException {
        bot.run();
    }
}