package serialization;

public enum  MediaType {
    XML,
    JSON;
}
