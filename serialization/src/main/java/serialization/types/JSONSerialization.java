package serialization.types;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.Request;
import dto.Response;
import serialization.Serialization;

public class JSONSerialization implements Serialization<Request, Response> {

    private final ObjectMapper objectMapper;

    public JSONSerialization() {
        super();
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public Request deserialize(String serialized) throws Exception {
        return objectMapper.readValue(serialized.getBytes(), Request.class);
    }

    @Override
    public String serialize(Response dto) throws Exception {
        return objectMapper.writeValueAsString(dto);
    }
}
