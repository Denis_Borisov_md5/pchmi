package serialization.types;

import dto.Request;
import dto.Response;
import serialization.Serialization;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public class XMLSerialization implements Serialization<Request, Response> {

    private final JAXBContext context;

    public XMLSerialization() throws JAXBException {
        super();
        this.context = JAXBContext.newInstance(Request.class, Response.class);
    }

    @Override
    public Request deserialize(String serialized) throws Exception {
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (Request) unmarshaller.unmarshal(new StringReader(serialized));
    }

    @Override
    public String serialize(Response dto) throws Exception {
        Marshaller marshaller = context.createMarshaller();
        StringWriter resultWriter = new StringWriter();
        marshaller.marshal(dto, resultWriter);
        return resultWriter.toString();
    }
}
