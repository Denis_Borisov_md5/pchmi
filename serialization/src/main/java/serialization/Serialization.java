package serialization;

public interface Serialization<Q, S> extends Deserializer<Q>, Serializer<S>{
}
