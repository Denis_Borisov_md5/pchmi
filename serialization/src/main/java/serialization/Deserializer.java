package serialization;

public interface Deserializer<T> {
    T deserialize(String serialized) throws Exception;
}
