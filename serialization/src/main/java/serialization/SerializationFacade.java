package serialization;

import dto.Request;
import dto.Response;

import java.util.HashMap;
import java.util.Map;

public class SerializationFacade {
    private Map<String, Serialization<Request, Response>> serializations = new HashMap<>();

    public Request deserialize(MediaType mediaType, String request) throws Exception {
        Deserializer<Request> deserializer = this.serializations.get(mediaType.toString());
        return deserializer.deserialize(request);
    }

    public String serialize(MediaType mediaType, Response response) throws Exception {
        Serializer<Response> serializer = this.serializations.get(mediaType.toString());
        return serializer.serialize(response);
    }

    public void registerMediaTypeSerialization(MediaType mediaType, Serialization<Request, Response> serialization) {
        this.serializations.put(mediaType.toString(), serialization);
    }
}
