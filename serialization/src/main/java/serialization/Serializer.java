package serialization;

public interface Serializer<T> {
    String serialize(T dto) throws Exception;
}
