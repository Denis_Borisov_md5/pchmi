package service;

import dto.Request;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CalculatorService {

    public List<BigDecimal> getSortedInputs(Request request) {
        Stream<BigDecimal> sumInputs = request.getSumInputs().stream()
                .filter(Objects::nonNull);
        Stream<BigDecimal> mulInputs = request.getMulInputs().stream()
                .filter(Objects::nonNull)
                .map(BigDecimal::new);
        return Stream.concat(sumInputs, mulInputs)
                .sorted().collect(Collectors.toList());
    }

    public BigDecimal calculateSum(Request request) {
        BigDecimal sum = request.getSumInputs().stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return sum.multiply(new BigDecimal(request.getRatio()));
    }

    public BigInteger calculateMul(Request request) {
        return request.getMulInputs().stream()
                .filter(Objects::nonNull)
                .reduce(BigInteger.ONE, BigInteger::multiply);
    }
}
