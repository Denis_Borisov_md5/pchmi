package service;

import dto.Request;
import dto.Response;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RequestProcessing {
    private CalculatorService calculatorService;

    public Response process(Request request) {
        Response response = new Response();
        response.setSumResult(calculatorService.calculateSum(request));
        response.setMulResult(calculatorService.calculateMul(request));
        response.setSortedInputs(calculatorService.getSortedInputs(request));
        return response;
    }
}
