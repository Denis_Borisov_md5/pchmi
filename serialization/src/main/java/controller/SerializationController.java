package controller;

import dto.Request;
import dto.Response;
import lombok.AllArgsConstructor;
import serialization.MediaType;
import serialization.SerializationFacade;
import service.RequestProcessing;

@AllArgsConstructor
public class SerializationController {
    private SerializationFacade serializationFacade;
    private RequestProcessing requestProcessing;

    public String handleRequest(MediaType mediaType, String request) throws Exception {
        Request requestDto = serializationFacade.deserialize(mediaType, request);
        Response responseDto = requestProcessing.process(requestDto);
        return serializationFacade.serialize(mediaType, responseDto);
    }
}
