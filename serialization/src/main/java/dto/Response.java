package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Data
@XmlRootElement(name = "Output")
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {
    @JsonProperty("SumResult")
    @XmlElement(name = "SumResult")
    private BigDecimal sumResult;

    @JsonProperty("MulResult")
    @XmlElement(name = "MulResult")
    private BigInteger mulResult;

    @JsonProperty("SortedInputs")
    @XmlElement(name = "decimal")
    @XmlElementWrapper(name = "SortedInputs")
    private List<BigDecimal> sortedInputs;
}
