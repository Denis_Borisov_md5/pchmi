package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Data
@XmlRootElement(name = "Input")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
    @JsonProperty("K")
    @XmlElement(name = "K")
    private BigInteger ratio;

    @JsonProperty("Sums")
    @XmlElement(name = "decimal")
    @XmlElementWrapper(name = "Sums")
    private List<BigDecimal> sumInputs;

    @JsonProperty("Muls")
    @XmlElement(name = "int")
    @XmlElementWrapper(name = "Muls")
    private List<BigInteger> mulInputs;
}
