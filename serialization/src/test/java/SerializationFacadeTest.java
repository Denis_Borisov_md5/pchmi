import dto.Request;
import dto.Response;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import serialization.MediaType;
import serialization.SerializationFacade;
import serialization.types.JSONSerialization;
import serialization.types.XMLSerialization;

@RunWith(JUnit4.class)
public class SerializationFacadeTest {
    private SerializationFacade sut;

    @Before
    public void setUp() throws Exception {
        sut = new SerializationFacade();
        sut.registerMediaTypeSerialization(MediaType.XML, new XMLSerialization());
        sut.registerMediaTypeSerialization(MediaType.JSON, new JSONSerialization());
    }

    @After
    public void tearDown() {
        sut = null;
    }

    @Test
    public void shouldSerializeToXML() throws Exception {
        shouldSerializeTo(MediaType.XML, TestRequestUtil.createResponse(), "response.xml");
    }

    @Test
    public void shouldSerializeToJSON() throws Exception {
        shouldSerializeTo(MediaType.JSON, TestRequestUtil.createResponse(), "response.json");
    }

    @Test
    public void shouldDeserializeFromXML() throws Exception {
        shouldDeserializeFrom(MediaType.XML, "request.xml", TestRequestUtil.createRequest());
    }

    @Test
    public void shouldDeserializeFromJSON() throws Exception {
        shouldDeserializeFrom(MediaType.JSON, "request.json", TestRequestUtil.createRequest());
    }

    private void shouldSerializeTo(MediaType mediaType, Response dto, String expectedResource)
            throws Exception {
        String expectedBody = TestResourceUtil.normalizeXml(TestResourceUtil.getResource(expectedResource));
        String serializedBody = sut.serialize(mediaType, dto);
        Assert.assertThat(serializedBody, new IsEqual<String>(expectedBody));
    }

    private void shouldDeserializeFrom(MediaType mediaType, String sourceResource, Request expectedDto)
            throws Exception {
        String sourceBody = TestResourceUtil.getResource(sourceResource);
        Request deserializedDto = sut.deserialize(mediaType, sourceBody);
        Assert.assertThat(deserializedDto, new IsEqual<Request>(expectedDto));
    }
}