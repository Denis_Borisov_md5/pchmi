import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@UtilityClass
public class TestResourceUtil {
    public String normalizeXml(String xmlString) {
        return xmlString.replaceAll("[\\n|\\t|\\r]", "");
    }

    public String getResource(String fileName) throws URISyntaxException, IOException {
        Path expectedXMLFilePath = Paths.get(TestRequestUtil.class.getClassLoader().getResource(fileName).toURI());
        return new String(Files.readAllBytes(expectedXMLFilePath));
    }
}