import dto.Request;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import service.CalculatorService;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class CalculatorServiceTest {

    private CalculatorService sut;

    @Before
    public void setUp() {
        sut = new CalculatorService();
    }

    @Test
    public void shouldReturnSortedByAscendingMergedFromSumAndMulInputs() {
        Request request = createRequestWithNullValues();
        List<BigDecimal> result = sut.getSortedInputs(request);
        Assert.assertThat(result, new IsEqual<List<BigDecimal>>(Arrays.asList(
                BigDecimal.valueOf(1),
                BigDecimal.valueOf(1.01),
                BigDecimal.valueOf(2.02),
                BigDecimal.valueOf(4)
        )));
    }

    @Test
    public void shouldReturnCalculatedSum() {
        Request request = createRequestWithNullValues();
        BigDecimal sum = sut.calculateSum(request);
        Assert.assertThat(sum, new IsEqual<BigDecimal>(new BigDecimal("30.30")));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenKoefficientIsNull() {
        Request request = TestRequestUtil.createRequest();
        request.setRatio(null);
        sut.calculateSum(request);
    }

    @Test
    public void shouldReturnCalculatedMul() {
        Request request = createRequestWithNullValues();
        BigInteger sum = sut.calculateMul(request);
        Assert.assertThat(sum, new IsEqual<BigInteger>(BigInteger.valueOf(4)));
    }

    private Request createRequestWithNullValues() {
        Request request = TestRequestUtil.createRequest();
        request.getSumInputs().add(null);
        request.getMulInputs().add(null);
        return request;
    }
}