import dto.Request;
import dto.Response;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import service.CalculatorService;
import service.RequestProcessing;

@RunWith(JUnit4.class)
public class RequestProcessingTest {
    private RequestProcessing sut;

    @Before
    public void setUp() {
        sut = new RequestProcessing(new CalculatorService());
    }

    @Test
    public void shouldReturnProcessedResponseDto() {
        Request request = TestRequestUtil.createRequest();
        Response response = sut.process(request);
        Assert.assertThat(response, new IsEqual<Response>(TestRequestUtil.createResponse()));
    }
}
