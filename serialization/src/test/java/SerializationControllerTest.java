import controller.SerializationController;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import serialization.MediaType;
import serialization.SerializationFacade;
import serialization.types.JSONSerialization;
import serialization.types.XMLSerialization;
import service.CalculatorService;
import service.RequestProcessing;

import javax.xml.bind.JAXBException;

@RunWith(JUnit4.class)
public class SerializationControllerTest {
    private SerializationController sut;

    @Before
    public void setUp() throws JAXBException {
        SerializationFacade serializer = new SerializationFacade();
        serializer.registerMediaTypeSerialization(MediaType.XML, new XMLSerialization());
        serializer.registerMediaTypeSerialization(MediaType.JSON, new JSONSerialization());
        RequestProcessing requestProcessor = new RequestProcessing(new CalculatorService());
        sut = new SerializationController(serializer, requestProcessor);
    }

    @Test
    public void shouldProcessXMLResponse() throws Exception {
        shouldProcessResponse(MediaType.XML, "request.xml", "response.xml");
    }

    @Test
    public void shouldProcessJSONResponse() throws Exception {
        shouldProcessResponse(MediaType.JSON, "request.json", "response.json");
    }

    private void shouldProcessResponse(MediaType mediaType, String requestResoruce, String responseResource) throws Exception {
        String request = TestResourceUtil.normalizeXml(TestResourceUtil.getResource(requestResoruce));
        String expected = TestResourceUtil.normalizeXml(TestResourceUtil.getResource(responseResource));
        String response = sut.handleRequest(mediaType, request);
        Assert.assertThat(response, new IsEqual<String>(expected));
    }
}
