import dto.Request;
import dto.Response;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class TestRequestUtil {
    public Request createRequest() {
        Request request = new Request();
        request.setRatio(BigInteger.valueOf(10));
        request.setSumInputs(createSumInputs());
        request.setMulInputs(createMulInputs());
        return request;
    }

    public Response createResponse() {
        Response response = new Response();
        response.setMulResult(BigInteger.valueOf(4));
        response.setSumResult(new BigDecimal("30.30"));
        response.setSortedInputs(createSortedInputs());
        return response;
    }

    private List<BigDecimal> createSumInputs() {
        List<BigDecimal> inputs = new ArrayList<BigDecimal>();
        inputs.add(BigDecimal.valueOf(1.01));
        inputs.add(BigDecimal.valueOf(2.02));
        return inputs;
    }

    private List<BigInteger> createMulInputs() {
        List<BigInteger> inputs = new ArrayList<BigInteger>();
        inputs.add(BigInteger.valueOf(1));
        inputs.add(BigInteger.valueOf(4));
        return inputs;
    }

    private List<BigDecimal> createSortedInputs() {
        List<BigDecimal> inputs = new ArrayList<BigDecimal>();
        inputs.add(new BigDecimal("1"));
        inputs.add(new BigDecimal("1.01"));
        inputs.add(new BigDecimal("2.02"));
        inputs.add(new BigDecimal("4"));
        return inputs;
    }
}