package restservice;

import dto.Request;
import dto.Response;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

@RestController
@SpringBootApplication
@RequestMapping(path = "/study")
@SuppressWarnings("deprecation")
public class RestService extends WebMvcConfigurerAdapter {
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        AntPathMatcher matcher = new AntPathMatcher();
        matcher.setCaseSensitive(false);
        configurer.setPathMatcher(matcher);
    }

    public static void main(String[] args) {
        SpringApplication.run(RestService.class, args);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/ping")
    public ResponseEntity<?> ping() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getInputData")
    public ResponseEntity<Request> getInputData() {
        Request inputData = new Request();
        inputData.setRatio(new BigInteger("10"));
        inputData.setMulInputs(Arrays.asList(
                new BigInteger("1"),
                new BigInteger("4")
        ));
        inputData.setSumInputs(Arrays.asList(
                new BigDecimal("1.01"),
                new BigDecimal("2.02")
        ));
        return new ResponseEntity<Request>(inputData, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path="/writeAnswer")
    public ResponseEntity<?> writeAnswer(@RequestBody Response answer) {
        if (!isAnswerValid(answer)) {
            new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean isAnswerValid(Response answer) {
        return answer.getMulResult().equals(new BigInteger("4")) &&
                answer.getSumResult().equals(new BigDecimal("30.30")) &&
                answer.getSortedInputs().equals(Arrays.asList(
                        new BigDecimal("1"),
                        new BigDecimal("1.01"),
                        new BigDecimal("2.02"),
                        new BigDecimal("4")
                ));
    }
}